<?php declare(strict_types=1);

namespace App\Person\Domain;

final class Person
{
    private int $id;

    private string $email;

    public function __construct(int $id, string $email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    public function findPersonsWithDeletedStatus()
    {
        $qb = new QueryBuilder();
        $qb->addSelect('p')
            ->where('p.status = :status')
            ->setParameter(['status' => 'DELETED']);
    }
}