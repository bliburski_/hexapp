<?php declare(strict_types=1);

namespace App\Person\Domain;

interface PersonRepositoryInterface
{
    public function findById(int $id): Person;
}