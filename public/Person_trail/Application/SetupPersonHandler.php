<?php declare(strict_types=1);

namespace App\Person\Application;

use App\Person\Domain\Person;
use App\Person\Domain\PersonRepositoryInterface;

final class SetupPersonHandler
{
    private PersonRepositoryInterface $persons;

    public function __construct(PersonRepositoryInterface $persons)
    {
        $this->persons = $persons;
    }

    public function __invoke(SetupPersonCommand $command)
    {
        $newPerson = new Person($command->getId(), $command->getEmail());
        //$this->persons->add($newPersons);
    }
}