<?php declare(strict_types=1);

namespace App\Person\Application;

interface PersonQueryInterface
{
    public function findByEmail(EmailValue $emailValue): PersonView;


}