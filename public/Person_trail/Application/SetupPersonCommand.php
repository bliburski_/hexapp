<?php declare(strict_types=1);

namespace App\Person\Application;

use App\Shared\Application\CommandInterface;

// DTO dla handlera

class SetupPersonCommand implements CommandInterface
{
    private int $id;
    private string $email;

    public function __construct(int $id, string $email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}