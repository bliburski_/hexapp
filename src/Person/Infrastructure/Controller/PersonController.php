<?php

namespace App\Person\Infrastructure\Controller;

use App\Person\Application\Command\CreatePersonCommand;
use App\Person\Application\Command\CreatePersonCommandHandler;
use App\Person\Application\Query\PersonListQuery;
use App\Person\Application\Query\PersonQuery;
use App\Shared\Infrastructure\SymfonySerializer;
use App\Shared\Infrastructure\SynchronousCommandBus;
use App\Shared\Infrastructure\SynchronousQueryBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{
    private CreatePersonCommandHandler $createPersonCommandHandler;
    private SymfonySerializer $serializer;

    public function __construct(
        CreatePersonCommandHandler $createPersonCommandHandler,
        SymfonySerializer $serializer
    )
    {
        $this->createPersonCommandHandler = $createPersonCommandHandler;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/person", methods={"GET"})
     **/
    public function getAllPersons(SynchronousQueryBus $queryBus, PersonListQuery $personQuery): JsonResponse
    {
        dump($queryBus->ask($personQuery));die; //todo serializacja :)
    }

    /**
     * @Route("/person/{ldap}", methods={"GET"})
     **/
    public function getPerson(SynchronousQueryBus $queryBus, PersonQuery $personQuery, string $ldap): JsonResponse
    {
        dump($queryBus->ask($personQuery, $ldap));die; //todo serializacja :)
    }

    /**
     * @Route("/person", methods={"POST"})
     **/
    public function createPerson(SynchronousCommandBus $bus)
    {
        $bus->map(CreatePersonCommand::class, $this->createPersonCommandHandler);
        $bus->handle(
            new CreatePersonCommand('bliburski', 'active') //todo deserializacja :)
        );

        return new Response('Dodano użytkownika');
    }
}