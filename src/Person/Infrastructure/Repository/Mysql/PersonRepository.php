<?php

declare(strict_types=1);

namespace App\Person\Infrastructure\Repository\Mysql;

use App\Person\Application\Query\PersonListView;
use App\Person\Application\Query\PersonView;
use App\Person\Domain\Exception\PersonNotFoundException;
use Doctrine\DBAL\Connection;

class PersonRepository
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll() : PersonListView
    {
        $sql = 'SELECT BIN_TO_UUID(id) as id, ldap, person_status FROM person';

        $personsData = $this->connection->fetchAllAssociative($sql);

        $personListView = new PersonListView();
        array_map(function(array $personData) use ($personListView) {
            $personListView->addPerson(new PersonView($personData['id'], $personData['ldap'], $personData['person_status']));
        }, $personsData);

        return $personListView;
    }

    public function getSinglePerson($ldap) : PersonView
    {
        $sql = "SELECT BIN_TO_UUID(id) as id, ldap, person_status FROM person WHERE ldap = '{$ldap}'";

        $personData = $this->connection->fetchAssociative($sql);
        if (!$personData) {
            throw new PersonNotFoundException('Person not found');
        }

        return new PersonView($personData['id'], $personData['ldap'], $personData['person_status']);
    }
}