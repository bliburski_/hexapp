<?php

namespace App\Person\Infrastructure\Repository\Doctrine;

use App\Person\Domain\Model\Person;
use Doctrine\ORM\EntityManagerInterface;

class PersonCreator
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createPerson(Person $person)
    {
        $this->em->persist($person);
        $this->em->flush();
    }
}