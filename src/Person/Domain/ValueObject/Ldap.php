<?php

declare(strict_types=1);

namespace App\Person\Domain\ValueObject;

use App\Shared\Domain\Exception\IncorrectValueObjectException;

final class Ldap
{
    private string $value;

    public function __construct(string $ldap)
    {
        $this->value = $ldap;
        $this->validateLdap();
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return (string) $this->getValue();
    }

    private function validateLdap(): void
    {
        if (strlen($this->value) < 4) {
            throw new IncorrectValueObjectException('Too short ldap');
        }

        if (strlen($this->value) > 50) {
            throw new IncorrectValueObjectException('Too long ldap');
        }
        //other validations...
    }
}