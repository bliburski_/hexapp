<?php

declare(strict_types=1);

namespace App\Person\Domain\ValueObject;

use App\Shared\Domain\ValueObject\Enum;

final class PersonStatus extends Enum
{
    public const ACTIVE = 'active';
    public const INACTIVE = 'inactive';

    protected function throwExceptionForInvalidValue($value)
    {
        throw new \Exception("Zła wartość statusu persona: $value");
    }
}