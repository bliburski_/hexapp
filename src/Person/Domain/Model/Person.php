<?php

declare(strict_types=1);

namespace App\Person\Domain\Model;

use App\Person\Domain\ValueObject\Ldap;
use App\Person\Domain\ValueObject\PersonStatus;
use Symfony\Component\Uid\Uuid;

class Person
{
    private Uuid $id;

    private Ldap $ldap;

    private PersonStatus $personStatus;

    public function __construct(Uuid $id, Ldap $ldap, PersonStatus $personStatus)
    {
        $this->id = $id;
        $this->ldap = $ldap;
        $this->personStatus = $personStatus;
    }
}