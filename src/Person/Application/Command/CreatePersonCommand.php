<?php

declare(strict_types=1);

namespace App\Person\Application\Command;

use App\Shared\Application\CommandInterface;

final class CreatePersonCommand implements CommandInterface
{
    private string $ldap;

    private string $personStatus;

    public function __construct(string $ldap, string $personStatus)
    {
        $this->ldap = $ldap;
        $this->personStatus = $personStatus;
    }

    public function getLdap(): string
    {
        return $this->ldap;
    }

    public function getPersonStatus(): string
    {
        return $this->personStatus;
    }
}