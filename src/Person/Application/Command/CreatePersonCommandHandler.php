<?php

declare(strict_types=1);

namespace App\Person\Application\Command;

use App\Person\Domain\Model\Person;
use App\Person\Domain\ValueObject\Ldap;
use App\Person\Domain\ValueObject\PersonStatus;
use App\Person\Infrastructure\Repository\Doctrine\PersonCreator;
use App\Shared\Application\CommandHandlerInterface;
use Symfony\Component\Uid\Uuid;

final class CreatePersonCommandHandler implements CommandHandlerInterface
{
    private PersonCreator $personCreator;

    public function __construct(PersonCreator $personCreator)
    {
        $this->personCreator = $personCreator;
    }

    public function __invoke(CreatePersonCommand $command)
    {
        $newPerson = new Person(
            Uuid::v4(),
            new Ldap($command->getLdap()),                  // każdy parametr jest encją lub ValueObject-em
            new PersonStatus($command->getPersonStatus())
        );

        $this->personCreator->createPerson($newPerson);
    }
}