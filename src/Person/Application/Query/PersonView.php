<?php

declare(strict_types=1);

namespace App\Person\Application\Query;

final class PersonView
{
    private string $id;

    private string $ldap;

    private string $personStatus;

    public function __construct(string $id, string $ldap, string $personStatus)
    {
        $this->id = $id;
        $this->ldap = $ldap;
        $this->personStatus = $personStatus;
    }
}