<?php

declare(strict_types=1);

namespace App\Person\Application\Query;

use App\Person\Infrastructure\Repository\Mysql\PersonRepository;

final class PersonListQuery
{
    private PersonRepository $personRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function __invoke(): PersonListView
    {
        return $this->personRepository->getAll();
    }
}