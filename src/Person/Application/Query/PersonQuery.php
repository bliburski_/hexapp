<?php

declare(strict_types=1);

namespace App\Person\Application\Query;

use App\Person\Infrastructure\Repository\Mysql\PersonRepository;

final class PersonQuery
{
    private PersonRepository $personRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function __invoke(string $ldap): PersonView
    {
        return $this->personRepository->getSinglePerson($ldap);
    }
}