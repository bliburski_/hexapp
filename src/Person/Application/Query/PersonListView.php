<?php

declare(strict_types=1);

namespace App\Person\Application\Query;

final class PersonListView
{
    private array $persons = [];

    public function addPerson(PersonView $personView)
    {
        $this->persons[] = $personView;
    }
}