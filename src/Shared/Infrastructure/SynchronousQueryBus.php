<?php declare(strict_types=1);

namespace App\Shared\Infrastructure;

final class SynchronousQueryBus
{
    public function ask($query, ...$parameters)
    {
        if ($parameters !== null && !empty($parameters)) {
            return call_user_func($query, ...$parameters);
        }

        return call_user_func($query);
    }
}