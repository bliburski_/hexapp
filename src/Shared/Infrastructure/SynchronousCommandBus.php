<?php declare(strict_types=1);

namespace App\Shared\Infrastructure;

use App\Shared\Application\CommandInterface;
use Exception;

final class SynchronousCommandBus implements CommandInterface
{
    private array $handlers;

    public function map(string $command, callable $handler): void
    {
        $this->handlers[$command] = $handler;
    }

    public function handle(CommandInterface $command): void
    {
        $fqcn = \get_class($command);
        if (false === isset($this->handlers[$fqcn])) {
            throw new Exception('Brak handlera do komendy');
        }

        call_user_func($this->handlers[$fqcn], $command);
    }
}