<?php

namespace App\Shared\Infrastructure;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import('../../../config/{packages}/*.yaml');
        $container->import('../../../config/{packages}/'.$this->environment.'/*.yaml');

        if (true) {
            $container->import('../../../config/services.yaml');
            $container->import('../../../config/{services}_'.$this->environment.'.yaml');
        } else {
            $container->import('../../../config/{services}.php');
        }
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('../../../config/{routes}/'.$this->environment.'/*.yaml');
        $routes->import('../../../config/{routes}/*.yaml');

        if (true) {
            $routes->import('../../../config/routes.yaml');
        } else {
            $routes->import('../../../config/{routes}.php');
        }
    }
}
