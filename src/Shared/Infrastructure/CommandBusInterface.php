<?php declare(strict_types=1);

namespace App\Shared\Infrastructure;

use App\Shared\Application\CommandInterface;

interface CommandBusInterface
{
    public function handle(CommandInterface $command): void;
}