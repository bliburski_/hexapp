up:
	docker-compose up -d

down:
	docker-compose down

login:
	docker exec -it -u dev hex_php bash

database-startup:
	docker exec hex_php php bin/console doctrine:database:drop --force
	docker exec hex_php php bin/console doctrine:database:create
	docker exec hex_php php bin/console doctrine:schema:update --force

cache-clear:
	docker exec hex_php php bin/console c:c

cache-warmup:
	docker exec hex_php php bin/console c:warmup

composer-install:
	docker exec hex_php composer install

container-debug:
	docker exec hex_php php bin/console debug:container '$(service)'

git-pull:
	git fetch
	git pull

git-push:
	git push origin HEAD

git-commit-push:
ifneq ($(msg),)
	git add .
	git commit -m "$(msg)"
	git push origin HEAD
else
	$(info ************ NO MESSAGE GIVEN! **********)
endif

.PHONY: fresh
fresh: cache-clear database-startup cache-warmup

.PHONY: rebuild
rebuild: composer-install database-startup cache-warmup